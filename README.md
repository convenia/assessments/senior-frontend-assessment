# Teste para pessoa desenvolvedora frontend senior - Convenia

A divisão de contas em restaurantes costuma ser trabalhosa para o garçom, e tem o potencial de gerar confusões.
O seu objetivo é criar um webapp para facilitar a vida desse garçom.

## Requisitos da aplicação
- O garçom pode selecionar a mesa para gerar a conta de cobrança.
- Ao selecionar a mesa, é exibida uma lista dos produtos consumidos, os respectivos preços e o valor total a ser pago.
- O garçom pode registrar os pagamentos individuais referentes à mesa, caso alguém queira sair antes.
- Devem ser exibidos os pagamentos parciais e o quanto falta para completar o valor total da mesa.

## Requisitos técnicos
- O aplicação deve ser preferencialmente desenvolvida em Vue.js 2, mas serão aceitos testes em Vue.js 3.
- Sinta-se livre para utilizar qualquer pacote de componentes que desejar, porém o desenvolvimento de seus próprios componentes será considerado como um adicional na sua avaliação.
- Recomendamos o uso do Vuex ou Pinia para a store.
- Incentivamos o uso de alguma metodologia para organização do seu CSS (RSCSS, BEM, etc).
- Indicamos também o uso de algum pré-processador de CSS (SCSS, SASS, Stylus, etc).
- Não é recomandada a utilização de frameworks para o design (bootstrap, tailwind, materialize, foundation, etc). A ideia é avaliar seus conhecimentos de css puro.
- O webapp deve ser responsivo.
- Desafio opcional: Implementar a visualização dos valores convertidos para Dolar e Euro utilizando nossa [API GraphQL de conversão de moedas](https://gitlab.com/convenia/assessments/currency-conversion).

## Referência visual

Os designs a seguir podem ser utilizados como referência para o seu desenvolvimento.
A responsividade mobile fica a seu cargo.
Sinta-se livre para alterar o design da forma como preferir. As imagens a seguir são apenas um esboço:


![Mockup Mesas](/Mesas.png)

![Mockup Pedido](/Pedido.png)

![Mockup Pagamento](/Pagamento.png)

## Importante
- Serão avaliadas clareza e organização do código.
- Não é obrigatória a implementação de backend.
- Os dados das mesas e produtos não precisam ser de fato registrados num banco de dados, podem ser dados "mockados".
- Transições e animações dos elementos são bem-vindas!
- Você tem duas semanas para realizar a avaliação.

Divirta-se, seja criativo(a) e mostre-nos do que é capaz!

### Aguardamos seu Merge Request

Convenia :purple_heart:
